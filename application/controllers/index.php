<?php
class indexController extends Controller {
	public function index() {
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('index', $this->data);
	}
	
	public function language($languageid = null) {
		if($this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$language = $this->functions->getBy('system_languages', array('language_id' => $languageid));
			
			setcookie('language', $language['language_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		} else {
			setcookie('language', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
	
	public function timezone($timezoneid = null) {
		if($this->functions->getTotal('system_timezones', array('timezone_id' => $timezoneid))) {
			$timezone = $this->functions->getBy('system_timezones', array('timezone_id' => $timezoneid));
			
			setcookie('timezone', $timezone['timezone_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		} else {
			setcookie('timezone', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
	
	public function currencie($currencieid = null) {
		if($this->functions->getTotal('system_currencies', array('currencie_id' => $currencieid))) {
			$currencie = $this->functions->getBy('system_currencies', array('currencie_id' => $currencieid));
			
			setcookie('currencie', $currencie['currencie_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		} else {
			setcookie('currencie', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
}
?>