<?php
class indexController extends Controller {
	public function index() {
		if(!$this->functions->isAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array();
		
		$getSearch = array();
		
		$getJoins = array();
		
		$getSort = array(
			'order_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['order_id'] = $this->request->get['q'];
			$getSearch['order_name'] = $this->request->get['q'];
			$getSearch['order_country'] = $this->request->get['q'];
			$getSearch['order_city'] = $this->request->get['q'];
			$getSearch['order_address'] = $this->request->get['q'];
			$getSearch['order_zipcode'] = $this->request->get['q'];
			$getSearch['order_phone'] = $this->request->get['q'];
			$getSearch['order_email'] = $this->request->get['q'];
			$getSearch['order_text'] = $this->request->get['q'];
			$getSearch['order_track'] = $this->request->get['q'];
			$getSearch['order_amount'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_orders', $getData, $getSearch, $getJoins);
		$orders = $this->functions->get('shop_orders', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/admin/shop/orders';
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['orders'] = $orders;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'orders'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopOrders'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopOrders'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/orders/index', $this->data);
	}
}
?>