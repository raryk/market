<?php
class footerController extends Controller {
	public function index() {
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('common/footer', $this->data);
	}
}
?>