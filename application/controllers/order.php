<?php
class orderController extends Controller {
	public function index($ordersecret = null) {
		if(!$this->functions->getTotal('shop_orders', array('order_secret' => $ordersecret))) {
			return $this->functions->config('404');
		}
		
		$order = $this->functions->getBy('shop_orders', array('order_secret' => $ordersecret));
		$this->data['order'] = $order;
		
		$section = array(
			'element' => 'order',
			'element_order' => $order['order_secret']
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleOrder'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionOrder'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('order', $this->data);
	}
	
	public function ajax($csrf = null, $ordersecret = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_secret' => $ordersecret))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrderTotal');
			return json_encode($this->data);
		}
		
		$order = $this->functions->getBy('shop_orders', array('order_secret' => $ordersecret));
		
		if(!$order['order_status']) {
			$data = array(
				'id' => 7,
				'amount' => $this->functions->convert($order['order_amount'], 2, $order['order_currencie']),
				'currencie' => 'RUB',
				'email' => $order['order_email'],
				'key' => $order['order_id'],
				'value' => 'Order payment #' . $order['order_id'],
				'pending' => $this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret'],
				'success' => $this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret'],
				'error' => $this->functions->config('connect') . $this->functions->config('domain') . '/order/index/' . $order['order_secret']
			);
			ksort($data, SORT_STRING);
			$data['secret'] = 'B6A75631682599B5';
			$signature = base64_encode(md5(implode(':', $data), true));
			unset($data['secret']);
			$data['signature'] = $signature;
			
			$this->data['status'] = 'success';
			$this->data['url'] = 'https://flyxen.com/pay?' . http_build_query($data);
			return json_encode($this->data);
		} else {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrder');
			return json_encode($this->data);
		}
	}
	
	public function delete($csrf = null, $ordersecret = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_secret' => $ordersecret))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrderTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_orders', array('order_secret' => $ordersecret));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
}
?>