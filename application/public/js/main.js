$(document).ready(function() {
	$(document).on('click', '[data-action="toggle"]', function(event) {
		event.preventDefault();
		
		$(this).toggleClass('active');
	});
	
	$(document).on('click', '[data-action="navigation"]', function(event) {
		event.preventDefault();
		
		$(this).closest('[data-navigation="parent"]').children('[data-navigation="slide"]').stop().slideToggle(250);
		$(this).closest('[data-navigation="parent"]').children('[data-navigation="toggle"]:not([data-navigation="slide"])').toggleClass('active');
		$(this).closest('[data-navigation="parent"]').children(':not([data-navigation="slide"])').find('[data-navigation="toggle"]').toggleClass('active');
	});
	
	$(document).on('click', '[data-action="gallery"]', function(event) {
		event.preventDefault();
		
		var gallery = $(document.createElement('div')).addClass('detail fade').data('ride', 'dialog').appendTo('body');
		
		var element = $(this);
		
		if($(this).closest('[data-gallery="parent"]').find('[data-action="gallery"]').length > 1) {
			var src = $(this).data('src');
			var title = $(this).closest('[data-gallery="parent"]').find('[data-action="gallery"]').index(this) + 1 + '/' + $(this).closest('[data-gallery="parent"]').find('[data-action="gallery"]').length;
			var item = '';
			
			$(this).closest('[data-gallery="parent"]').find('[data-action="gallery"]').each(function(index) {
				if($(element).closest('[data-gallery="parent"]').find('[data-action="gallery"]').index(element) == index) {
					if($(this).data('item') == 'video') {
						item += '<div class="carousel-item active galleryCarouselItem" data-content="carousel">\
							<video poster="' + $(this).data('poster') + '" class="gallery">\
								<source src="' + $(this).data('src') + '" type="' + $(this).data('type') + '" class="gallerySrc">\
							</video>\
						</div>';
					} else {
						item += '<div class="carousel-item active galleryCarouselItem" data-content="carousel">\
							<img src="' + $(this).data('src') + '" alt="' + $(this).data('alt') + '" class="gallery gallerySrc">\
						</div>';
					}
				} else {
					if($(this).data('item') == 'video') {
						item += '<div class="carousel-item galleryCarouselItem" data-content="carousel">\
							<video poster="' + $(this).data('poster') + '" class="gallery">\
								<source src="' + $(this).data('src') + '" type="' + $(this).data('type') + '" class="gallerySrc">\
							</video>\
						</div>';
					} else {
						item += '<div class="carousel-item galleryCarouselItem" data-content="carousel">\
							<img src="' + $(this).data('src') + '" alt="' + $(this).data('alt') + '" class="gallery gallerySrc">\
						</div>';
					}
				}
			});
			
			gallery.html('<div class="expand">\
				<div class="control">\
					<div class="control-addon">\
						<button type="button" class="btn second" data-dismiss="dialog">\
							<i class="zmdi zmdi-arrow-left"></i>\
						</button>\
					</div>\
					<div class="control-block">\
						<div class="model">\
							<span class="galleryTitle">' + title + '</span>\
						</div>\
					</div>\
					<div class="control-addon">\
						<a href="' + src + '" class="btn second galleryDownload">\
							<i class="zmdi zmdi-download"></i>\
						</a>\
					</div>\
				</div>\
			</div>\
			<div class="inner galleryCarousel" data-ride="carousel" data-interval="0">\
				<div class="carousel">'
					+ item +
				'</div>\
				<i class="indicator" data-slide="prev"></i>\
				<i class="indicator next" data-slide="next"></i>\
			</div>');
		} else {
			if($(this).data('item') == 'video') {
				var src = $(this).data('src');
				var preview = '<video poster="' + $(this).data('poster') + '" class="gallery">\
					<source src="' + $(this).data('src') + '" type="' + $(this).data('type') + '">\
				</video>';
			} else {
				var src = $(this).data('src');
				var preview = '<img src="' + $(this).data('src') + '" alt="' + $(this).data('alt') + '" class="gallery">';
			}
			
			gallery.html('<div class="expand">\
				<div class="control">\
					<div class="control-block">\
						<button type="button" class="btn second" data-dismiss="dialog">\
							<i class="zmdi zmdi-arrow-left"></i>\
						</button>\
					</div>\
					<div class="control-addon">\
						<a href="' + src + '" class="btn second">\
							<i class="zmdi zmdi-download"></i>\
						</a>\
					</div>\
				</div>\
			</div>'
			+ preview);
		}
		
		$('[data-ride="carousel"]').carousel();
		
		$('.galleryCarousel').on('slid.bs.carousel', function(event) {
			$('.galleryTitle').text($('.active.galleryCarouselItem').index() + 1 + '/' + $('.galleryCarouselItem').length);
			$('.galleryDownload').attr('href', $('.active.galleryCarouselItem').find('.gallerySrc').attr('src'));
		});
		
		gallery.one('hidden.bs.dialog', function() {
			$('.galleryCarousel').off('slid.bs.carousel');
			
			this.remove();
		});
		
		gallery.dialog({
			'back': false
		});
	});
	
	$(document).on('click', 'a[href^="#"][data-action="scroll"]', function(event) {
		if($($(this).attr('href')).length) {
			event.preventDefault();
			
			$(this).scrollParent().animate({
				scrollTop: $($(this).attr('href')).offset().top - $(this).scrollParent().offset().top + $(this).scrollParent().scrollTop()
			}, 250);
		}
	});
	
	if($('.mainFarbtastic').length) {
		$('.mainFarbtastic').each(function() {
			$($(this).find('.mainFarbtasticContent')).farbtastic($(this).find('.mainFarbtasticControl'));
		});
	}
	
	if($('.mainSortable').length) {
		$('.mainSortable').each(function() {
			Sortable.create(this, {
				handle: '.mainSortableHandle',
				animation: 250
			});
		});
	}
	
	if($('.mainSummernote').length) {
		$('.mainSummernote').summernote({
			height: 360
		});
	}
});