<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/timezones/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $timezone['timezone_id'] ?>" method="POST" class="widget editTimezone">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_SystemTimezonesEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_SystemTimezonesEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<div class="menu hover" data-target="#system_timezone_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_SystemTimezonesIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formKey">
							<div class="above">
								<label for="key" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemTimezonesLabelKey') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="key" placeholder="<?php echo $functions->languageInit('Admin_SystemTimezonesFormKey') ?>" value="<?php echo $timezone['timezone_key'] ?>" class="form block" id="key">
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="value" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemTimezonesLabelValue') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="value[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_SystemTimezonesFormValue') ?>" value="<?php echo isset(json_decode($timezone['timezone_value'], true)[$item['language_id']]) ? json_decode($timezone['timezone_value'], true)[$item['language_id']] : null ?>" class="form block" id="value">
							</div>
							<?php endforeach; ?>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemTimezonesEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="system_timezone_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_SystemTimezonesIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_SystemTimezonesIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_SystemTimezonesIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteTimezone">
								<span><?php echo $functions->languageInit('Admin_SystemTimezonesIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editTimezone', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonTimezone').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonTimezone">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteTimezone', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/system/timezones/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($timezone['timezone_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/timezones';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>