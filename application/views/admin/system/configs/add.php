<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/configs/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addConfig">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemConfigsAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemConfigsAddDesc') ?></span>
							</div>
						</div>
						<div class="over formKey">
							<div class="above">
								<label for="key" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemConfigsLabelKey') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="key" placeholder="<?php echo $functions->languageInit('Admin_SystemConfigsFormKey') ?>" class="form block" id="key">
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="value" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemConfigsLabelValue') ?></span>
								</label>
							</div>
							<div class="above">
								<textarea rows="5" cols="1" name="value" placeholder="<?php echo $functions->languageInit('Admin_SystemConfigsFormValue') ?>" class="form block" id="value"></textarea>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemConfigsAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addConfig', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonConfig').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonConfig">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/configs';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>