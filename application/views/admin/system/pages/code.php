<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $language['language_value'] ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $language['language_key'] ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/admin/system/pages/code/index/<?php echo $language['language_id'] ?>/<?php echo $page['page_id'] ?>">
		<meta property="og:description" content="<?php echo $language['language_value'] ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $language['language_key'] ?></title>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/admin/system/pages/code/index/<?php echo $language['language_id'] ?>/<?php echo $page['page_id'] ?>?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/admin/system/pages/code/index/<?php echo $language['language_id'] ?>/<?php echo $page['page_id'] ?>" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/admin/system/pages/code/index/<?php echo $language['language_id'] ?>/<?php echo $page['page_id'] ?>?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/ace/ace.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<header class="header">
				<div class="control">
					<div class="control-addon">
						<a href="/admin/system/pages/edit/index/<?php echo $page['page_id'] ?>" class="btn second">
							<i class="zmdi zmdi-arrow-left"></i>
						</a>
					</div>
					<div class="control-block">
						<div class="model">
							<span><?php echo $language['language_key'] ?></span>
						</div>
						<div class="model">
							<span class="focus"><?php echo $language['language_value'] ?></span>
						</div>
					</div>
					<form action="/admin/system/pages/code/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $language['language_id'] ?>/<?php echo $page['page_id'] ?>" method="POST" class="control-addon codePage" style="display:none;">
						<textarea name="code" class="hidden"><?php echo isset(json_decode($page['page_code'], true)[$language['language_id']]) ? json_decode($page['page_code'], true)[$language['language_id']] : null ?></textarea>
						<button type="submit" class="btn">
							<i class="zmdi zmdi-check"></i>
						</button>
					</form>
				</div>
			</header>
			<section id="code"></section>
		</div>
		<script>
			$('#code').height(window.innerHeight - $('.header').outerHeight(true));
			
			$(window).on('resize', function() {
				$('#code').height(window.innerHeight - $('.header').outerHeight(true));
			});
			
			var value = $('textarea[name="code"]').val();
			var edit = ace.edit('code', {
				mode: 'ace/mode/html'
			});
			
			edit.focus();
			edit.getSession().setValue(value);
			edit.getSession().on('change', function() {
				$('textarea[name="code"]').val(edit.getSession().getValue());
				
				if(value == edit.getSession().getValue()) {
					$('.codePage').hide();
				} else {
					$('.codePage').show();
				}
			});
			
			$(document).on('submit', '.codePage', function(event) {
				event.preventDefault();
				
				var form = $(this);
				
				$.ajax({
					contentType: false,
					processData: false,
					type: form.attr('method'),
					url: form.attr('action'),
					data: new FormData(form[0]),
					beforeSend: function(data) {
						form.find('button[type="submit"]').prop('disabled', true);
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								$.growl({
									message: data.error,
									type: 'error'
								});
								break;
							case 'success':
								$.growl({
									message: data.success,
									type: 'success'
								});
								
								if(value != edit.getSession().getValue()) {
									value = edit.getSession().getValue();
									form.hide();
								}
								break;
						}
						
						form.find('button[type="submit"]').prop('disabled', false);
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						form.find('button[type="submit"]').prop('disabled', false);
					}
				});
			});
		</script>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>