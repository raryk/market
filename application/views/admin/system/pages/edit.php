<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/pages/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $page['page_id'] ?>" method="POST" class="widget editPage">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_SystemPagesEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<a href="/<?php echo $page['page_address'] ?>" class="menu">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_SystemPagesIndexPage') ?></span>
											</div>
										</a>
										<div class="menu hover" data-target="#system_page_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_SystemPagesIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formAddress">
							<div class="above">
								<label for="address" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelAddress') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<span><?php echo $functions->config('domain') ?>/</span>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="address" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormAddress') ?>" value="<?php echo $page['page_address'] ?>" class="form block" id="address">
									</div>
								</div>
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormTitle') ?>" value="<?php echo isset(json_decode($page['page_title'], true)[$item['language_id']]) ? json_decode($page['page_title'], true)[$item['language_id']] : null ?>" class="form block" id="value">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormDescription') ?>" value="<?php echo isset(json_decode($page['page_description'], true)[$item['language_id']]) ? json_decode($page['page_description'], true)[$item['language_id']] : null ?>" class="form block" id="value">
							</div>
							<?php endforeach; ?>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemPagesEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemPagesEditCode') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesEditCodeDesc') ?></span>
							</div>
						</div>
						<?php foreach($functions->get('system_languages') as $item): ?>
						<a href="/admin/system/pages/code/index/<?php echo $item['language_id'] ?>/<?php echo $page['page_id'] ?>" class="menu">
							<div class="control">
								<div class="control-addon">
									<div class="btn media">
										<i class="zmdi zmdi-code"></i>
									</div>
								</div>
								<div class="control-block">
									<div class="name">
										<span><?php echo $item['language_key'] ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $item['language_value'] ?></span>
									</div>
								</div>
							</div>
						</a>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="over">
					<form action="/admin/system/pages/edit/section/<?php echo $functions->getCsrf() ?>/<?php echo $page['page_id'] ?>" method="POST" class="widget editPage">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemPagesEditSection') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesEditSectionDesc') ?></span>
							</div>
						</div>
						<div class="over mainSortable">
							<?php foreach(json_decode($page['page_section'], true) as $item): ?>
							<div class="above pageSection">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="section[key][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormSectionKey') ?>" value="<?php echo $item['key'] ?>" class="form block">
									</div>
									<div class="control-block">
										<input type="text" name="section[value][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormSectionValue') ?>" value="<?php echo $item['value'] ?>" class="form block">
									</div>
									<div class="control-addon">
										<button type="button" class="btn error removeSection">
											<i class="zmdi zmdi-close"></i>
										</button>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
							<div class="above formSection">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="section[key][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormSectionKey') ?>" class="form block inputSectionKey">
									</div>
									<div class="control-block">
										<input type="text" name="section[value][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormSectionValue') ?>" class="form block inputSectionValue">
									</div>
									<div class="control-addon">
										<button type="button" class="btn info addSection">
											<i class="zmdi zmdi-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemPagesEditSectionSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/system/pages/edit/common/<?php echo $functions->getCsrf() ?>/<?php echo $page['page_id'] ?>" method="POST" class="widget editPage">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemPagesEditCommon') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesEditCommonDesc') ?></span>
							</div>
						</div>
						<div class="over mainSortable">
							<?php foreach(json_decode($page['page_common'], true) as $item): ?>
							<div class="above pageCommon">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="common[key][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormCommonKey') ?>" value="<?php echo $item['key'] ?>" class="form block">
									</div>
									<div class="control-block">
										<input type="text" name="common[value][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormCommonValue') ?>" value="<?php echo $item['value'] ?>" class="form block">
									</div>
									<div class="control-addon">
										<button type="button" class="btn error removeCommon">
											<i class="zmdi zmdi-close"></i>
										</button>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
							<div class="above formCommon">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="common[key][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormCommonKey') ?>" class="form block inputCommonKey">
									</div>
									<div class="control-block">
										<input type="text" name="common[value][]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormCommonValue') ?>" class="form block inputCommonValue">
									</div>
									<div class="control-addon">
										<button type="button" class="btn info addCommon">
											<i class="zmdi zmdi-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemPagesEditCommonSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="system_page_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_SystemPagesIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_SystemPagesIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deletePage">
								<span><?php echo $functions->languageInit('Admin_SystemPagesIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editPage', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonPage').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonPage">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.addSection', function() {
						$('.formSection').before('<div class="above pageSection">\
							<div class="control">\
								<div class="control-addon">\
									<div class="btn media mainSortableHandle">\
										<i class="zmdi zmdi-arrows"></i>\
									</div>\
								</div>\
								<div class="control-block">\
									<input type="text" name="section[key][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormSectionKey')) ?>" value="' + $('.inputSectionKey').val() + '" class="form block">\
								</div>\
								<div class="control-block">\
									<input type="text" name="section[value][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormSectionValue')) ?>" value="' + $('.inputSectionValue').val() + '" class="form block">\
								</div>\
								<div class="control-addon">\
									<button type="button" class="btn error removeSection">\
										<i class="zmdi zmdi-close"></i>\
									</button>\
								</div>\
							</div>\
						</div>');
						
						$('.formSection').html('<div class="control">\
							<div class="control-addon">\
								<div class="btn media mainSortableHandle">\
									<i class="zmdi zmdi-arrows"></i>\
								</div>\
							</div>\
							<div class="control-block">\
								<input type="text" name="section[key][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormSectionKey')) ?>" class="form block inputSectionKey">\
							</div>\
							<div class="control-block">\
								<input type="text" name="section[value][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormSectionValue')) ?>" class="form block inputSectionValue">\
							</div>\
							<div class="control-addon">\
								<button type="button" class="btn info addSection">\
									<i class="zmdi zmdi-plus"></i>\
								</button>\
							</div>\
						</div>');
					});
					$(document).on('click', '.removeSection', function() {
						$(this).closest('.pageSection').remove();
					});
					
					$(document).on('click', '.addCommon', function() {
						$('.formCommon').before('<div class="above pageCommon">\
							<div class="control">\
								<div class="control-addon">\
									<div class="btn media mainSortableHandle">\
										<i class="zmdi zmdi-arrows"></i>\
									</div>\
								</div>\
								<div class="control-block">\
									<input type="text" name="common[key][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormCommonKey')) ?>" value="' + $('.inputCommonKey').val() + '" class="form block">\
								</div>\
								<div class="control-block">\
									<input type="text" name="common[value][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormCommonValue')) ?>" value="' + $('.inputCommonValue').val() + '" class="form block">\
								</div>\
								<div class="control-addon">\
									<button type="button" class="btn error removeCommon">\
										<i class="zmdi zmdi-close"></i>\
									</button>\
								</div>\
							</div>\
						</div>');
						
						$('.formCommon').html('<div class="control">\
							<div class="control-addon">\
								<div class="btn media mainSortableHandle">\
									<i class="zmdi zmdi-arrows"></i>\
								</div>\
							</div>\
							<div class="control-block">\
								<input type="text" name="common[key][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormCommonKey')) ?>" class="form block inputCommonKey">\
							</div>\
							<div class="control-block">\
								<input type="text" name="common[value][]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemPagesFormCommonValue')) ?>" class="form block inputCommonValue">\
							</div>\
							<div class="control-addon">\
								<button type="button" class="btn info addCommon">\
									<i class="zmdi zmdi-plus"></i>\
								</button>\
							</div>\
						</div>');
					});
					$(document).on('click', '.removeCommon', function() {
						$(this).closest('.pageCommon').remove();
					});
					
					$(document).on('click', '.deletePage', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/system/pages/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($page['page_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/pages';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>