<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/pages/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addPage">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemPagesAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemPagesAddDesc') ?></span>
							</div>
						</div>
						<div class="over formAddress">
							<div class="above">
								<label for="address" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelAddress') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<span><?php echo $functions->config('domain') ?>/</span>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="address" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormAddress') ?>" class="form block" id="address">
									</div>
								</div>
							</div>
						</div>
						<div class="over formTitle">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormTitle') ?>" class="form block" id="title">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemPagesLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_SystemPagesFormDescription') ?>" class="form block" id="value">
							</div>
							<?php endforeach; ?>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemPagesAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addPage', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonPage').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonPage">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/pages';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>