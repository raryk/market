<?php echo $header ?>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_Index') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_IndexDesc') ?></span>
							</div>
						</div>
						<?php if($functions->getTotal('shop_orders', array('order_status' => 1))): ?>
						<div class="over">
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableName') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableAmount') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($functions->get('shop_orders', array('order_status' => 1), array(), array(), array('order_id' => 'DESC'), array('start' => 0, 'limit' => 5)) as $item): ?>
										<tr>
											<td>
												<div class="model">
													<span><?php echo $item['order_id'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $item['order_name'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->currencieInit($item['order_currencie'], $item['order_amount'], true) ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['order_date_add'])) ?></span>
												</div>
											</td>
											<td>
												<button type="button" class="btn default" data-toggle="dialog">
													<span><?php echo $functions->languageInit('CommonAction') ?></span>
													<span>&nbsp;</span>
													<i class="zmdi zmdi-caret-down"></i>
												</button>
												<div class="dropdown fade" data-ride="dialog" data-position="true">
													<a href="/admin/shop/orders/view/index/<?php echo $item['order_id'] ?>" class="menu">
														<div class="model">
															<span><?php echo $functions->languageInit('Admin_ShopOrdersIndexView') ?></span>
														</div>
													</a>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<a href="/admin/shop/orders" class="btn default block">
							<span><?php echo $functions->languageInit('CommonMore') ?></span>
						</a>
						<?php else: ?>
						<div class="text-center">
							<div class="above">
								<div class="btn media">
									<i class="zmdi zmdi-alert-triangle"></i>
								</div>
							</div>
							<div class="name">
								<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
<?php echo $footer ?>