<?php echo $header ?>
				<form action="/admin/guard/logs" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/guard/logs" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_GuardLogs') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_GuardLogsDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($logs)): ?>
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_GuardLogsTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_GuardLogsTableIp') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_GuardLogsTableAgent') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_GuardLogsTableDate') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($logs as $item): ?>
										<tr>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['log_id']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['log_ip']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['log_agent']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['log_date_add'])) ?></span>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
<?php echo $footer ?>