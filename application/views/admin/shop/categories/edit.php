<?php echo $header ?>
				<div class="over">
					<form action="/admin/shop/categories/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $categorie['categorie_id'] ?>" method="POST" class="widget editCategorie">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_ShopCategoriesEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_ShopCategoriesEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<a href="/categorie/index/<?php echo $categorie['categorie_id'] ?>" class="menu">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_ShopCategoriesIndexPage') ?></span>
											</div>
										</a>
										<div class="menu hover" data-target="#shop_categorie_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_ShopCategoriesIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formTitle">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopCategoriesFormTitle') ?>" value="<?php echo isset(json_decode($categorie['categorie_title'], true)[$item['language_id']]) ? json_decode($categorie['categorie_title'], true)[$item['language_id']] : null ?>" class="form block" id="title">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formDescription">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopCategoriesFormDescription') ?>" class="form block" id="description"><?php echo isset(json_decode($categorie['categorie_description'], true)[$item['language_id']]) ? json_decode($categorie['categorie_description'], true)[$item['language_id']] : null ?></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelStatus') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="status" class="custom"<?php if($categorie['categorie_status']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopCategoriesEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="shop_categorie_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_ShopCategoriesIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_ShopCategoriesIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_ShopCategoriesIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteCategorie">
								<span><?php echo $functions->languageInit('Admin_ShopCategoriesIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editCategorie', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonCategorie').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonCategorie">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteCategorie', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/shop/categories/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($categorie['categorie_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/categories';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>