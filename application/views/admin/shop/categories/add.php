<?php echo $header ?>
				<div class="over">
					<form action="/admin/shop/categories/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addCategorie">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_ShopCategoriesAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_ShopCategoriesAddDesc') ?></span>
							</div>
						</div>
						<div class="over formTitle">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopCategoriesFormTitle') ?>" class="form block" id="title">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formDescription">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopCategoriesFormDescription') ?>" class="form block" id="description"></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCategoriesLabelStatus') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="status" class="custom">
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopCategoriesAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addCategorie', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonCategorie').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonCategorie">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/categories';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>