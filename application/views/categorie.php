<?php echo $header ?>
				<form action="/categorie/index/<?php echo $categorie['categorie_id'] ?>" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/categorie/index/<?php echo $categorie['categorie_id'] ?>" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo isset(json_decode($categorie['categorie_title'], true)[$functions->language()]) ? json_decode($categorie['categorie_title'], true)[$functions->language()] : json_decode($categorie['categorie_title'], true)[$functions->config('language')] ?></span>
							</div>
							<div class="name">
								<?php foreach($functions->string(nl2br(isset(json_decode($categorie['categorie_description'], true)[$functions->language()]) ? json_decode($categorie['categorie_description'], true)[$functions->language()] : json_decode($categorie['categorie_description'], true)[$functions->config('language')]), true) as $item): ?>
								<?php if(isset($item['link'])): ?>
								<a href="<?php echo $item['link'] ?>" class="link">
									<span class="focus"><?php echo $item['link'] ?></span>
								</a>
								<?php else: ?>
								<span class="focus"><?php echo $item['text'] ?></span>
								<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
						<?php if(!empty($functions->get('shop_categories', array('categorie_status' => 1)))): ?>
						<div class="over">
							<div class="wrap">
								<?php foreach($functions->get('shop_categories', array('categorie_status' => 1)) as $item): ?>
								<div class="wrap-item">
									<a href="/categorie/index/<?php echo $item['categorie_id'] ?>" class="btn small background-color-transparent color-inherit<?php if($categorie['categorie_id'] == $item['categorie_id']): ?> active<?php endif; ?>">
										<span><?php echo isset(json_decode($item['categorie_title'], true)[$functions->language()]) ? json_decode($item['categorie_title'], true)[$functions->language()] : json_decode($item['categorie_title'], true)[$functions->config('language')] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
						<div class="over">
							<?php if(!empty($products)): ?>
							<div class="row">
								<?php foreach($products as $item): ?>
								<div class="col-3-sm col-6-xs">
									<div class="forward">
										<div class="banner">
											<?php if(isset(json_decode($item['product_image'], true)[0]) && !empty($functions->image(json_decode($item['product_image'], true)[0]))): ?>
											<img src="<?php echo $functions->image(json_decode($item['product_image'], true)[0])['p'] ?>" alt="image">
											<?php else: ?>
											<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
											<?php endif; ?>
										</div>
										<div class="over">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<?php foreach($functions->string(isset(json_decode($item['product_title'], true)[$functions->language()]) ? json_decode($item['product_title'], true)[$functions->language()] : json_decode($item['product_title'], true)[$functions->config('language')]) as $string): ?>
														<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
														<?php endforeach; ?>
													</div>
													<div class="model">
														<?php if($item['product_discount']): ?>
														<span class="text-line"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'], $functions->currencie())) ?></span>
														<span>&nbsp;</span>
														<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'] * (100 - $item['product_discount']) / 100, $functions->currencie())) ?></span>
														<?php else: ?>
														<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'], $functions->currencie())) ?></span>
														<?php endif; ?>
													</div>
												</div>
												<div class="control-addon">
													<a href="/product/index/<?php echo $item['product_id'] ?>" class="btn second">
														<i class="zmdi zmdi-laptop"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endforeach; ?>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
<?php echo $footer ?>