<?php
class Document {
	private $title;
	private $description;
	private $section;
	private $url;
	private $canonical;
	private $alternate;
	
	public function getTitle() {
		return $this->title;
	}
	
	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function setDescription($description) {
		$this->description = $description;
	}
	
	public function getSection() {
		return $this->section;
	}
	
	public function setSection($section) {
		$this->section = $section;
	}
	
	public function getUrl() {
		return $this->url;
	}
	
	public function setUrl($url) {
		$this->url = $url;
	}
	
	public function getCanonical() {
		return $this->canonical;
	}
	
	public function setCanonical($canonical) {
		$this->canonical = $canonical;
	}
	
	public function getAlternate() {
		return $this->alternate;
	}
	
	public function setAlternate($alternate) {
		$this->alternate = $alternate;
	}
}
?>